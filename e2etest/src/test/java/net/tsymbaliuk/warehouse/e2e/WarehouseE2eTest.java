package net.tsymbaliuk.warehouse.e2e;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.PortBinding;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.NoConnectionReuseStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.hamcrest.collection.IsMapContaining;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;

public class WarehouseE2eTest {
    private static final Logger log = LoggerFactory.getLogger(WarehouseE2eTest.class);

    private static final String imageName = "docker.io/library/wh-server:0.0.1-SNAPSHOT";
    private static final String port = "8080";

    private static final Gson gson = new Gson();

    protected static String containerId;
    protected static DockerClient docker;
    protected static LogStream logs;

    private static boolean shouldStartContainer = Boolean.valueOf(Objects.requireNonNullElse(System.getenv("TEST_START_DOCKER"), "true"));

    @BeforeAll
    public static void setUp() throws DockerException, InterruptedException, DockerCertificateException {
//        log.info("System properties: {}", System.getProperties());
        if (!shouldStartContainer) {
            return;
        }
        docker = DefaultDockerClient.fromEnv().build();

        startContainer();
        if (!waitBootToStart()) {
            throw new DockerException("Cannot start container");
        }
    }

    @AfterAll
    public static void tearDown() throws DockerException, InterruptedException {
        if (!shouldStartContainer) {
            return;
        }
        log.info("Shutting down container {}", containerId);
        docker.stopContainer(containerId, 3);
        log.info("Container {} stopped", containerId);
        // print container logs
        logs = docker.logs(containerId,
                DockerClient.LogsParam.follow(),
                DockerClient.LogsParam.timestamps(),
                DockerClient.LogsParam.stdout(),
                DockerClient.LogsParam.stderr());

        log.info("Container logs:");
        logs.forEachRemaining(lm -> log.info(StandardCharsets.UTF_8.decode(lm.content()).toString()));

        log.info("Removing container {}", containerId);
        // Remove container
        docker.removeContainer(containerId);
        log.info("Container {} removed", containerId);
        // Close the docker client
        docker.close();
    }

    @Test
    public void testGetReport() throws InterruptedException, IOException {
        Type objectType = new TypeToken<Map<String, Double>>() {}.getType();

        Map<String, Double> reportItemsActual = getJsonObject(objectType, "http://localhost:8080/report");
        log.info("Testing 'http://localhost:8080/report', received JSON: '{}'", reportItemsActual);
        assertThat(reportItemsActual, IsMapContaining.hasValue(37.0));
        assertThat(reportItemsActual, IsMapContaining.hasValue(53.0));
    }

    @Test
    public void testGetOrder() throws InterruptedException, IOException, JSONException {
        log.info("Testing \"http://localhost:8080/order/\"");
        String actual = getStringObject("http://localhost:8080/order/");
        log.info("Received JSON: '{}'", actual);

        String expected = "[" +
                "{\"id\":14," +
                "\"orderItems\":[" +
                "{\"product\":{\"id\":13,\"name\":\"Nails\",\"price\":7.0,\"sku\":\"HG-Nl-05\"},\"quantity\":9.0}," +
                "{\"product\":{\"id\":10,\"name\":\"Samsung\",\"price\":750.0,\"sku\":\"EL-SS-02\"},\"quantity\":6.0}," +
                "{\"product\":{\"id\":5,\"name\":\"Philadelphia\",\"price\":5.0,\"sku\":\"GR-PH-02\"},\"quantity\":2.0}," +
                "{\"product\":{\"id\":4,\"name\":\"PepperJack\",\"price\":10.0,\"sku\":\"GR-PJ-01\"},\"quantity\":1.0}" +
                "]," +
                "\"totalAmount\":18.0" +
                "}," +
                "{\"id\":15,\"orderItems\":[" +
                "{\"product\":{\"id\":7,\"name\":\"Cheddar\",\"price\":10.0,\"sku\":\"GR-CH-04\"},\"quantity\":3.0}," +
                "{\"product\":{\"id\":9,\"name\":\"iPhone\",\"price\":800.0,\"sku\":\"EL-IP-01\"},\"quantity\":5.0}," +
                "{\"product\":{\"id\":11,\"name\":\"Screw Driver\",\"price\":75.0,\"sku\":\"HG-WP-03\"},\"quantity\":7.0}," +
                "{\"product\":{\"id\":8,\"name\":\"Olive Oil\",\"price\":10.0,\"sku\":\"GR-OO-05\"},\"quantity\":4.0}" +
                "]," +
                "\"totalAmount\":19.0" +
                "}," +
                "{\"id\":16," +
                "\"orderItems\":[" +
                "{\"product\":{\"id\":12,\"name\":\"Chain Saw\",\"price\":200.0,\"sku\":\"HG-CS-04\"},\"quantity\":8.0}" +
                "]," +
                "\"totalAmount\":8.0" +
                "}," +
                "{\"id\":17," +
                "\"orderItems\":[" +
                "{\"product\":{\"id\":5,\"name\":\"Philadelphia\",\"price\":5.0,\"sku\":\"GR-PH-02\"},\"quantity\":1.0}," +
                "{\"product\":{\"id\":13,\"name\":\"Nails\",\"price\":7.0,\"sku\":\"HG-Nl-05\"},\"quantity\":9.0}," +
                "{\"product\":{\"id\":8,\"name\":\"Olive Oil\",\"price\":10.0,\"sku\":\"GR-OO-05\"},\"quantity\":4.0}," +
                "{\"product\":{\"id\":9,\"name\":\"iPhone\",\"price\":800.0,\"sku\":\"EL-IP-01\"},\"quantity\":5.0}," +
                "{\"product\":{\"id\":11,\"name\":\"Screw Driver\",\"price\":75.0,\"sku\":\"HG-WP-03\"},\"quantity\":7.0}," +
                "{\"product\":{\"id\":10,\"name\":\"Samsung\",\"price\":750.0,\"sku\":\"EL-SS-02\"},\"quantity\":6.0}," +
                "{\"product\":{\"id\":6,\"name\":\"WhitePotato\",\"price\":1.0,\"sku\":\"GR-WP-03\"},\"quantity\":2.0}," +
                "{\"product\":{\"id\":12,\"name\":\"Chain Saw\",\"price\":200.0,\"sku\":\"HG-CS-04\"},\"quantity\":8.0}," +
                "{\"product\":{\"id\":7,\"name\":\"Cheddar\",\"price\":10.0,\"sku\":\"GR-CH-04\"},\"quantity\":3.0}" +
                "]," +
                "\"totalAmount\":45.0" +
                "}]";

        log.info("Checking match expected {}", expected);
        JSONArray ordersActualJson = new JSONArray(actual);
        JSONAssert.assertEquals(expected, ordersActualJson, false);
    }

    @Test
    public void testSearchOrdersByProductName() throws InterruptedException, IOException, JSONException {
        log.info("Testing 'http://localhost:8080/order/search/?word=oil'");
        String ordersActualString = getStringObject("http://localhost:8080/order/search/?word=oil");
        log.info("Received JSON: '{}'", ordersActualString);

        String expected = "[{\"id\":15," +
                "\"orderItems\":[" +
                "{\"product\":{\"id\":9,\"name\":\"iPhone\",\"price\":800.0,\"sku\":\"EL-IP-01\"},\"quantity\":5.0}," +
                "{\"product\":{\"id\":11,\"name\":\"Screw Driver\",\"price\":75.0,\"sku\":\"HG-WP-03\"},\"quantity\":7.0}," +
                "{\"product\":{\"id\":8,\"name\":\"Olive Oil\",\"price\":10.0,\"sku\":\"GR-OO-05\"},\"quantity\":4.0}," +
                "{\"product\":{\"id\":7,\"name\":\"Cheddar\",\"price\":10.0,\"sku\":\"GR-CH-04\"},\"quantity\":3.0}]," +
                "\"totalAmount\":19.0}," +
                "{\"id\":17," +
                "\"orderItems\":[" +
                "{\"product\":{\"id\":6,\"name\":\"WhitePotato\",\"price\":1.0,\"sku\":\"GR-WP-03\"},\"quantity\":2.0}," +
                "{\"product\":{\"id\":5,\"name\":\"Philadelphia\",\"price\":5.0,\"sku\":\"GR-PH-02\"},\"quantity\":1.0}," +
                "{\"product\":{\"id\":7,\"name\":\"Cheddar\",\"price\":10.0,\"sku\":\"GR-CH-04\"},\"quantity\":3.0}," +
                "{\"product\":{\"id\":11,\"name\":\"Screw Driver\",\"price\":75.0,\"sku\":\"HG-WP-03\"},\"quantity\":7.0}," +
                "{\"product\":{\"id\":9,\"name\":\"iPhone\",\"price\":800.0,\"sku\":\"EL-IP-01\"},\"quantity\":5.0}," +
                "{\"product\":{\"id\":13,\"name\":\"Nails\",\"price\":7.0,\"sku\":\"HG-Nl-05\"},\"quantity\":9.0}," +
                "{\"product\":{\"id\":8,\"name\":\"Olive Oil\",\"price\":10.0,\"sku\":\"GR-OO-05\"},\"quantity\":4.0}," +
                "{\"product\":{\"id\":10,\"name\":\"Samsung\",\"price\":750.0,\"sku\":\"EL-SS-02\"},\"quantity\":6.0}," +
                "{\"product\":{\"id\":12,\"name\":\"Chain Saw\",\"price\":200.0,\"sku\":\"HG-CS-04\"},\"quantity\":8.0}]," +
                "\"totalAmount\":45.0}]";
        log.info("Comparing to expected '{}'", expected);
        JSONArray actualOrders = new JSONArray(ordersActualString);
        JSONAssert.assertEquals(
                expected,
                actualOrders, false);
    }

    protected static void startContainer() throws DockerException, InterruptedException {
        final Map<String, List<PortBinding>> portBindings = Map.of(
                "8080", List.of(PortBinding.of("0.0.0.0", port)));

        // Create container with exposed ports
        final ContainerConfig containerConfig = ContainerConfig.builder()
                .hostConfig(HostConfig.builder().portBindings(portBindings).build())
                .image(imageName)
                .exposedPorts(Set.of(port))
                .build();

        log.info("Creating container with config {}", containerConfig);
        final ContainerCreation creation = docker.createContainer(containerConfig);
        containerId = creation.id();
        log.info("Container created, id={}", containerId);
        // Start container
        log.info("Starting container {}", containerId);
        docker.startContainer(containerId);
        log.info("Container {} started", containerId);
    }

    private static boolean waitBootToStart() throws InterruptedException {
        long deadline = System.currentTimeMillis() + 20000; // timeout 20 sec
        while (System.currentTimeMillis() < deadline) {
            log.info("Checking service ready...");
            try {
                String res = getStringObject("http://localhost:8080/status/");
                if ("yes".equalsIgnoreCase(res)) {
                    log.info("Service ready.");
                    return true;
                }
            } catch(Exception e) {
                log.debug("Caught exception checking the service readiness", e);
            }
            Thread.sleep(500);
            log.info("Service is not yet ready");
        }

        return false;
    }

    private static CloseableHttpResponse httpGet(final String url,
                                                 CloseableHttpClient httpClient) throws IOException {
        HttpGet httpRequest = new HttpGet(url);
        httpRequest.addHeader("Content-Type", "application/json");

        return httpClient.execute(httpRequest);
    }

    private static CloseableHttpClient getHttpClient() {
        return HttpClients.custom()
                .setRetryHandler(new DefaultHttpRequestRetryHandler(0, false))
                .setConnectionReuseStrategy(NoConnectionReuseStrategy.INSTANCE)
                .build();
    }

    protected static <T> T getJsonObject(Type type, String url) throws IOException {
        return gson.fromJson(getStringObject(url), type);
    }

    protected static String getStringObject(String url) throws IOException {
        try (CloseableHttpClient client = getHttpClient();
             CloseableHttpResponse response = httpGet(url, client)){
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return new String(response.getEntity().getContent().readAllBytes(), "UTF-8");
            }
        }
        return null;
    }
}
