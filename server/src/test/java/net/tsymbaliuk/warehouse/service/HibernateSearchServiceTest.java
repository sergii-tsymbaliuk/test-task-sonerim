package net.tsymbaliuk.warehouse.service;

import net.tsymbaliuk.warehouse.pojos.db.Order;
import net.tsymbaliuk.warehouse.pojos.db.OrderItem;
import net.tsymbaliuk.warehouse.pojos.db.Product;
import org.apache.lucene.search.Query;
import org.hibernate.Session;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class HibernateSearchServiceTest {
    @Mock
    private EntityManager entityManager;
    @Mock
    private EntityManagerFactory entityManagerFactory;
    @Mock
    private Session session;

    private HibernateSearchService service;

    @BeforeEach
    public void setUp() {
        when(entityManager.getEntityManagerFactory()).thenReturn(entityManagerFactory);
        when(entityManagerFactory.createEntityManager()).thenReturn(entityManager);
        when(entityManager.unwrap(any())).thenReturn(session);
        service = new HibernateSearchService(entityManager);
    }

    @Test
    void getOrderByProductName() {
        Product p1 = new Product("P1", 20.0, "SKU1", null);
        p1.setId(1L);
        Product p2 = new Product("P2", 30.0, "SKU2", null);
        p2.setId(2L);

        Order o1 = new Order();
        o1.setId((110L));

        Order o2 = new Order();
        o2.setId((120L));

        OrderItem oi1 = new OrderItem(o1, p1, 5.0);
        o1.addOrderItem(oi1);
        OrderItem oi2 = new OrderItem(o2, p2, 6.0);
        o2.addOrderItem(oi2);

        p1.getOrderItems().add(oi1);
        p2.getOrderItems().add(oi2);

        List<Product> products = List.of(p1, p2);

        List<Order> orders = service.getOrdersByProducts(products);

        assertThat(orders, containsInAnyOrder(o1, o2));
    }
}