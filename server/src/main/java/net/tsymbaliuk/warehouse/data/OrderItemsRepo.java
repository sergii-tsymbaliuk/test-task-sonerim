package net.tsymbaliuk.warehouse.data;

import net.tsymbaliuk.warehouse.pojos.db.OrderItem;
import org.springframework.data.repository.CrudRepository;

public interface OrderItemsRepo extends CrudRepository<OrderItem, Long> {
}
