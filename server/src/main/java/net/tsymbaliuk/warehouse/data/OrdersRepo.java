package net.tsymbaliuk.warehouse.data;

import net.tsymbaliuk.warehouse.pojos.db.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrdersRepo extends CrudRepository<Order, Long> {
}
