package net.tsymbaliuk.warehouse.api;

import com.google.common.collect.ImmutableList;
import net.tsymbaliuk.warehouse.data.ProductRepo;
import net.tsymbaliuk.warehouse.pojos.db.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {
    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductRepo productRepo;

    @GetMapping(path="/product", produces = "application/json")
    public ResponseEntity<?> getProduct() {
        ResponseEntity<?> resp;
        try {
            List<Product> products = ImmutableList.copyOf(productRepo.findAll());
            return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path="/product/{productId}", produces = "application/json")
    public ResponseEntity<?> getProduct(@PathVariable("productId") Long productId) {
        ResponseEntity<?> resp;
        try {
            Optional<Product> product = productRepo.findById(productId);
            if (product.isEmpty()) {
                resp = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }else {
                resp = new ResponseEntity<Product>(product.get(), HttpStatus.OK);
            }
        } catch (Exception e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resp;
    }
}