package net.tsymbaliuk.warehouse.api;

import com.google.common.collect.ImmutableList;
import net.tsymbaliuk.warehouse.pojos.api.CreateOrderRequest;
import net.tsymbaliuk.warehouse.pojos.api.RequestOrderItem;
import net.tsymbaliuk.warehouse.pojos.db.Order;
import net.tsymbaliuk.warehouse.service.HibernateSearchService;
import net.tsymbaliuk.warehouse.service.ProductNotFoundException;
import net.tsymbaliuk.warehouse.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class OrderController {
    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private HibernateSearchService hibernateSearchService;

    @PostMapping(path="/order", produces = "application/json")
    public ResponseEntity<?> postOrder(@RequestBody CreateOrderRequest createOrderRequest) {
        ResponseEntity<?> resp;
        List<RequestOrderItem> requestOrderItems = createOrderRequest.getRequestOrderItems();
        log.info("Received post order for {}", Arrays.toString(requestOrderItems.toArray()));
        try {
            final Order order = orderService.postOrder(requestOrderItems);
            log.info("Created order number {}", order);
            resp = new ResponseEntity<>(order, HttpStatus.CREATED);
        } catch (ProductNotFoundException e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.NOT_FOUND);
        } catch (TransactionException e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.CONFLICT);
        } catch (Exception e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resp;
    }

    @GetMapping(path="/order", produces = "application/json")
    public ResponseEntity<?> getOrder() {
        ResponseEntity<?> resp;
        try {
            resp = new ResponseEntity<List<Order>>(
                    ImmutableList.copyOf(orderService.getOrders()),
                    HttpStatus.OK);
        } catch (Exception e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resp;
    }

    @GetMapping(path="/order/{orderId}", produces = "application/json")
    public ResponseEntity<?> getOrder(@PathVariable("orderId") Long orderId) {
        ResponseEntity<?> resp;
        try {
            Order order = orderService.getOrder(orderId);
            if (order == null) {
                resp = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }else {
                resp = new ResponseEntity<Order>(order, HttpStatus.OK);
            }
        } catch (Exception e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resp;
    }

    @GetMapping("/order/search/")
    public List<Order> searchOrders(@RequestParam("word") String searchString) {
        return hibernateSearchService.getOrderByProductName(searchString);
    }
}