package net.tsymbaliuk.warehouse.api;

import com.google.common.collect.Streams;
import net.tsymbaliuk.warehouse.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class ReportController {
    private static final Logger log = LoggerFactory.getLogger(ReportController.class);

    @Autowired
    private OrderService orderService;

    @GetMapping(path="/report", produces = "application/json")
    public ResponseEntity<?> getReport() {
        ResponseEntity<?> resp;

        log.info("Building report");
        Map<String, Double> ordersReport =
                Streams.stream(orderService.getOrders())
                        .map(o -> Pair.of(
                                o.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE),
                                o.getTotalAmount()))
                        .collect(Collectors.groupingBy(
                                Pair::getFirst,
                                Collectors.summingDouble(Pair::getSecond)));

        return new ResponseEntity<>(ordersReport, HttpStatus.OK);
    }
}