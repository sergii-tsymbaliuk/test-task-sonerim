package net.tsymbaliuk.warehouse.service;

import com.google.common.collect.ImmutableList;
import net.tsymbaliuk.warehouse.pojos.db.Order;
import net.tsymbaliuk.warehouse.pojos.db.OrderItem;
import net.tsymbaliuk.warehouse.pojos.db.Product;
import org.apache.lucene.search.Query;
import org.hibernate.Session;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class HibernateSearchService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public HibernateSearchService(EntityManager entityManager) {
        entityManager = entityManager.getEntityManagerFactory().createEntityManager();
        Session session = (Session) entityManager.unwrap(Session.class);
        this.entityManager = entityManager;
    }

    public List<Order> getOrderByProductName(String word) {
        return getOrdersByProducts(searchProductsByName(word));
    }

    protected List<Product> searchProductsByName(String word) {
        FullTextEntityManager fullTextEntityManager = getFullTextEntityManager(entityManager);
        QueryBuilder qb = fullTextEntityManager
                .getSearchFactory()
                .buildQueryBuilder()
                .forEntity(Product.class)
                .get();

        Query query = qb.keyword()
                .onFields("name")
                .matching(word)
                .createQuery();

        FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, Product.class);
        return fullTextQuery.getResultList();
    }

    protected List<Order> getOrdersByProducts(List<Product> products)
    {
        Set<Order> orders = new HashSet<>();
        for (Product product : products) {
            for (OrderItem orderItem : product.getOrderItems())
            orders.add(orderItem.getOrder());
        }
        return ImmutableList.copyOf(orders);
    }

    private static FullTextEntityManager getFullTextEntityManager(EntityManager entityManager) {
        return Search.getFullTextEntityManager(entityManager);
    }
}