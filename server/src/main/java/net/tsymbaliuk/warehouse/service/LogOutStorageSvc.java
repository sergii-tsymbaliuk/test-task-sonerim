package net.tsymbaliuk.warehouse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public class LogOutStorageSvc implements StorageService {
    private static final Logger log = LoggerFactory.getLogger(LogOutStorageSvc.class);

    @Override
    public void store(MultipartFile file) {
        try {
            log.info("Uploaded file:/n{}",
                    new String(file.getBytes(), "UTF-8"));
        } catch (IOException e) {
            log.error("Unable read file", e);
        }

    }
}
