package net.tsymbaliuk.warehouse.service;

import net.tsymbaliuk.warehouse.pojos.xml.DeviceInfo;
import net.tsymbaliuk.warehouse.pojos.xml.GetPages;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EpaperRequest {
    DeviceInfo deviceInfo;
    GetPages getPages;
}
