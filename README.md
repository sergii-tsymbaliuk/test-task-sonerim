# Test Task description

### Task Definition

Task description 
1) Create a Spring-boot service application with REST CRUD methods for the following entities: Category, Product, Order and OrderItem. 
    - Category contains: name and a list of products. 
    - Product contains: price, sku and name. 
    - Order Item contains: “quantity” value and has connection to Product.
    - Order contains: a list of order items and a total amount of the order. 
2) Add report controller, which returns date and sum of income for each day in JSON format (2016-08-22: 250.65, 2016-08.23: 571.12 ... etc). 
3) Integrate ElasticSearch and add “search” method, which must return the list of orders by part of the product name. 

Technology stack:

- Spring-boot
- Hibernate
- Liquibase
- PostgreSQL 

**Will be a plus:** 

- add appropriate tests to REST methods; 
- add integration with Swagger.


### Prerequisites
You need installed docker daemon on the machine you are going to run test task
* The test configuration creates/runs docker container on local host 
* The test connects to the exposed port 8080 on localhost   

### Instruction how to run
By default the e2e automatically creates and brings up docker container with image ```"docker.io/library/wh-server:0.0.1-SNAPSHOT"```
and maps its port 8080 to localhost:8080.
Hence you need to have docker installed on local machine, or override this logic and disable automatic container creation by setting env var ```TEST_START_DOCKER=false```  
1. Run e2e test with docker container managed by e2e test: 
    * clone the repo<br>```git clone git@gitlab.com:sergii-tsymbaliuk/bymdev-test-task.git```
    * build the image <br>```./gradlew clean :wh-server:bootBuildImage```
    * Execute end to end test:<br>```./gradlew :wh-e2etest:check -i```
1. Run e2e test without automatically managing docker container: 
    * clone the repo<br>
        ```git clone git@gitlab.com:sergii-tsymbaliuk/bymdev-test-task.git```
    * run service as a docker container with service:
        - build the image:<br>```./gradlew clean :wh-server:bootBuildImage```
        - start container:<br>```docker run -ti -p 8080:8080 wh-server:0.0.1-SNAPSHOT```
    * or run service as a spring boot application:<br>```./gradlew :wh-server:bootRun -i```
    * Execute end to end test:<br>```./gradlew :wh-e2etest:check -i```

### Open API & swagger
[swager ui](http://localhost:8080/swagger-ui/index.html?configUrl=/api-docs/swagger-config)<br>
[open api](http://localhost:8080/api-docs)<br>
[open api yaml](http://localhost:8080/api-docs.yaml)

####Author
[Sergii Tsymbaliuk](mailto:sergii@tsymbaliuk.net)



