package net.tsymbaliuk.warehouse.pojos.api;

import java.util.ArrayList;
import java.util.List;

public class CreateOrderRequest {

    private List<RequestOrderItem> requestOrderItems;

    public CreateOrderRequest() {
        this.requestOrderItems = new ArrayList<>();
    }

    public void addItemToOrder(Long productCode, Double quantity) {
        requestOrderItems.add(new RequestOrderItem(productCode, quantity));
    }

    public List<RequestOrderItem> getRequestOrderItems() {
        return List.copyOf(requestOrderItems);
    }
}
