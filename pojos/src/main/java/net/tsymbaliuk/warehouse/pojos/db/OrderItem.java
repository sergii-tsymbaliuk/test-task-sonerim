package net.tsymbaliuk.warehouse.pojos.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
public class OrderItem {

    @EmbeddedId
    private OrderItemId id = new OrderItemId();

    @ManyToOne
    @MapsId("orderId")
    @JoinColumn(name = "order_id")
    private Order order;

    @ManyToOne
    @MapsId("productId")
    private Product product;

    private Double quantity;

    public OrderItem() {
    }

    public OrderItem(Order order, Product product, Double quantity) {
        this.order = order;
        this.product = product;
        this.quantity = quantity;
    }

    public Double getQuantity() {
        return quantity;
    }

    @JsonBackReference
    public Order getOrder() {
        return order;
    }

    public Product getProduct() {
        return product;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                ", order=" + order.getId() +
                ", product=" + product.getId() +
                ", quantity=" + quantity +
                '}';
    }
}
