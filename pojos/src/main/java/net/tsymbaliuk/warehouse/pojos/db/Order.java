package net.tsymbaliuk.warehouse.pojos.db;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

@Entity
@Table(name = "warehouseOrders")
public class Order {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @OneToMany(mappedBy="order",
//            cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE},
            fetch = FetchType.EAGER
    )
    @JsonManagedReference
    private Set<OrderItem> orderItems = new HashSet<>();

    private Long timeStamp;

    public Order() {
        this.timeStamp = Instant.now().toEpochMilli();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<OrderItem> getOrderItems() {
        return List.copyOf(orderItems);
    }

    public void addOrderItem(OrderItem orderItem) {
        this.orderItems.add(orderItem);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderNumber=" + id +
                ", orderItems=" + ((orderItems == null || orderItems.isEmpty()) ? "none" : orderItems) +
                '}';
    }

    public Double getTotalAmount() {
        AtomicReference<Double> total = new AtomicReference<>(0.0);
        orderItems.forEach(oi -> total.updateAndGet(v -> v + oi.getQuantity()));
        return total.get();
    }

    public LocalDate getDate() {
        return Instant.ofEpochMilli(timeStamp).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
