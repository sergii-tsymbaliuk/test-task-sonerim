package net.tsymbaliuk.warehouse.pojos.xml;

import javax.xml.bind.annotation.XmlAttribute;

public class GetPages {
    @XmlAttribute
    String editionDefId;

    @XmlAttribute
    String publicationDate;
}
