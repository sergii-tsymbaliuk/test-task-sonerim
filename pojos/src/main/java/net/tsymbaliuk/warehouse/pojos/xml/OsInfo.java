package net.tsymbaliuk.warehouse.pojos.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OsInfo {
    @XmlAttribute
    String name;
    @XmlAttribute
    Integer version;
}
