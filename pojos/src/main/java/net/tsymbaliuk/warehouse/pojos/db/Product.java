package net.tsymbaliuk.warehouse.pojos.db;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TermVector;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Indexed
public class Product {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "name", scope = String.class)
    private Category category;

    @Field(store=Store.COMPRESS, name = "name", termVector = TermVector.YES)
    private String name;
    private Double price;
    private String sku;


    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private Set<OrderItem> orderItems = new HashSet<>();

    public Product() {
    }

    public Product(String name, Double price, String sku, Category category) {
        this.name = name;
        this.price = price;
        this.sku = sku;
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @JsonIgnore
    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", category=" + ((category == null) ? "none" : category.getName()) +
                ", name='" + name + '\'' +
                ", sku='" + sku + '\'' +
                ", price=" + price +
                '}';
    }

    public String toShortString() {
        return String.format(
                "%d - '%s', sku: %s, price: %f.2",
                id, name, sku, price);
    }
}
