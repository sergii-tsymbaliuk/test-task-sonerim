package net.tsymbaliuk.warehouse.pojos.xml;

import com.sun.xml.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AppInfo {
    String newspaperName;
    Integer version;
}
