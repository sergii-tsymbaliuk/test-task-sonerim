package net.tsymbaliuk.warehouse.pojos.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ScreenInfo {
    @XmlAttribute
    Integer width;
    @XmlAttribute
    Integer height;
    @XmlAttribute
    Integer dpi;
}
